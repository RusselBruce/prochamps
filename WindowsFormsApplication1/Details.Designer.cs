﻿namespace WindowsFormsApplication1
{
    partial class Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelReg = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblMuniName = new System.Windows.Forms.Label();
            this.lblOccupancy = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblServType = new System.Windows.Forms.Label();
            this.lblProd = new System.Windows.Forms.Label();
            this.lblPOD = new System.Windows.Forms.Label();
            this.lblRegCond = new System.Windows.Forms.Label();
            this.lblPropID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Property Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Property ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(41, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Property Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Municipality Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(41, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Occupancy";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Service Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(41, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "FC/FCH Codes";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(41, 299);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "Product";
            // 
            // labelReg
            // 
            this.labelReg.AutoSize = true;
            this.labelReg.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReg.Location = new System.Drawing.Point(41, 374);
            this.labelReg.Name = "labelReg";
            this.labelReg.Size = new System.Drawing.Size(52, 23);
            this.labelReg.TabIndex = 8;
            this.labelReg.Text = "Label";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(41, 335);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 23);
            this.label10.TabIndex = 9;
            this.label10.Text = "POD/Portfolio";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(293, 119);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(81, 19);
            this.lblAddress.TabIndex = 11;
            this.lblAddress.Text = "Property ID";
            // 
            // lblMuniName
            // 
            this.lblMuniName.AutoSize = true;
            this.lblMuniName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMuniName.Location = new System.Drawing.Point(293, 154);
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Size = new System.Drawing.Size(81, 19);
            this.lblMuniName.TabIndex = 12;
            this.lblMuniName.Text = "Property ID";
            // 
            // lblOccupancy
            // 
            this.lblOccupancy.AutoSize = true;
            this.lblOccupancy.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOccupancy.Location = new System.Drawing.Point(293, 190);
            this.lblOccupancy.Name = "lblOccupancy";
            this.lblOccupancy.Size = new System.Drawing.Size(81, 19);
            this.lblOccupancy.TabIndex = 13;
            this.lblOccupancy.Text = "Property ID";
            this.lblOccupancy.Click += new System.EventHandler(this.label13_Click);
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.Location = new System.Drawing.Point(293, 227);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(81, 19);
            this.lblCode.TabIndex = 14;
            this.lblCode.Text = "Property ID";
            // 
            // lblServType
            // 
            this.lblServType.AutoSize = true;
            this.lblServType.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServType.Location = new System.Drawing.Point(293, 264);
            this.lblServType.Name = "lblServType";
            this.lblServType.Size = new System.Drawing.Size(81, 19);
            this.lblServType.TabIndex = 15;
            this.lblServType.Text = "Property ID";
            // 
            // lblProd
            // 
            this.lblProd.AutoSize = true;
            this.lblProd.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProd.Location = new System.Drawing.Point(293, 303);
            this.lblProd.Name = "lblProd";
            this.lblProd.Size = new System.Drawing.Size(81, 19);
            this.lblProd.TabIndex = 16;
            this.lblProd.Text = "Property ID";
            // 
            // lblPOD
            // 
            this.lblPOD.AutoSize = true;
            this.lblPOD.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPOD.Location = new System.Drawing.Point(293, 338);
            this.lblPOD.Name = "lblPOD";
            this.lblPOD.Size = new System.Drawing.Size(81, 19);
            this.lblPOD.TabIndex = 17;
            this.lblPOD.Text = "Property ID";
            // 
            // lblRegCond
            // 
            this.lblRegCond.AutoSize = true;
            this.lblRegCond.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegCond.Location = new System.Drawing.Point(293, 374);
            this.lblRegCond.Name = "lblRegCond";
            this.lblRegCond.Size = new System.Drawing.Size(81, 19);
            this.lblRegCond.TabIndex = 18;
            this.lblRegCond.Text = "Property ID";
            // 
            // lblPropID
            // 
            this.lblPropID.Location = new System.Drawing.Point(297, 82);
            this.lblPropID.Name = "lblPropID";
            this.lblPropID.ReadOnly = true;
            this.lblPropID.Size = new System.Drawing.Size(210, 20);
            this.lblPropID.TabIndex = 19;
            // 
            // Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 494);
            this.Controls.Add(this.lblPropID);
            this.Controls.Add(this.lblRegCond);
            this.Controls.Add(this.lblPOD);
            this.Controls.Add(this.lblProd);
            this.Controls.Add(this.lblServType);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lblOccupancy);
            this.Controls.Add(this.lblMuniName);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelReg);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Details";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Property Details";
            this.Load += new System.EventHandler(this.Details_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelReg;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblMuniName;
        private System.Windows.Forms.Label lblOccupancy;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblServType;
        private System.Windows.Forms.Label lblProd;
        private System.Windows.Forms.Label lblPOD;
        private System.Windows.Forms.Label lblRegCond;
        private System.Windows.Forms.TextBox lblPropID;
    }
}