﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class ProChamp
    {
        public string id { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Property_Name { get; set; }
        public string Property_City { get; set; }
        public string Property_Address { get; set; }
        public string Registrant { get; set; }
        public string Investor { get; set; }
        public string OMT { get; set; }
        public string AMS { get; set; }
        public string PM { get; set; }
        public string PI_FC_Status { get; set; }
        public string PI_OC_Status { get; set; }
        public string PI_Last_Date { get; set; }

        public string handleTime { get; set; }
        public string Uid { get; set; }
    }


}
