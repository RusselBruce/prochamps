﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        //When dropdown change
        private void cbRegistration_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbRegistration.Text == "Successfully Padlocked" || 
                cbRegistration.Text == "Already Padlocked" ||
                cbRegistration.Text == "Unlocked by Prochamps"||
                cbRegistration.Text == "Reviewed by Prochamps")
            {
                tbProdId.Enabled = true;
                tbProdId.Clear();
            }
            else
            {
                tbProdId.Enabled = false;
                tbProdId.Clear();
            }
        }
        //Save Button
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");
            string qry1 = "";

            string StatusTag = "";
            string ChampTag = "";

            if (cbRegistration.Text != "")
            {
                if (cbRegistration.Text == "Successfully Padlocked" || cbRegistration.Text == "Already Padlocked")
                {
                    StatusTag = "Pending";
                }
                else if (cbRegistration.Text == "Already Registered" || cbRegistration.Text == "Continuing Registration" || cbRegistration.Text == "Not for Prochamps")
                {
                    StatusTag = "Completed";
                }
                else if (cbRegistration.Text == "Unlocked by Prochamps" || cbRegistration.Text == "Reviewed by Prochamps")
                {
                    StatusTag = "";
                }
                else if (cbRegistration.Text == "Registration Condition Not Met")
                {
                    StatusTag = "Completed";
                    cls.ExecuteQuery("update tbl_VPR_CreatedFile set [Registration Status] = 'Suspended' where id = '" + Form1.uid + "'");
                }
                else if (cbRegistration.Text == "Dispute Submitted" || cbRegistration.Text == "Need book and page")
                {
                    StatusTag = "Impediment";
                    cls.ExecuteQuery("update tbl_VPR_CreatedFile set [Registration Status] = 'On Hold' where id = '" + Form1.uid + "'");
                }
                ChampTag = cbRegistration.Text;

                qry1 = "update tbl_VPR_CreatedFile set status='" + StatusTag + "',processedBy='" + Environment.UserName
                        + "',[PRO ID]='" + tbProdId.Text + "',InitialProcessor='" + Environment.UserName + "',processedDate='"
                        + DateTime.Now + "',HandleTime=" + Form1.handleTime + ",[Prochamp Status] = '" + ChampTag + "' where id='" + Form1.uid + "'";
                cls.ExecuteQuery(qry1);

                this.Close();

            }
            else
            {
                MessageBox.Show("Please select status!");
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
