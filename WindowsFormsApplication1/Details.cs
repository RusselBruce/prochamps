﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Details : Form
    {
        public Details()
        {
            InitializeComponent();
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
        //Load the Property Details
        private void Details_Load(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");

            DataTable dt = new DataTable();
            //dt = cls.GetData("select top 1 * from view_VPR_Inflow where status in ('')" +
            //    " and [Registration Process] not in ('','Online') and lockto in ('','NULL') ORDER BY [Date Loaded]");

            dt = cls.GetData("select top 1 * from view_VPR_Inflow where lockto = '" + Environment.UserName + "' ");

            lblPropID.Text = dt.Rows[0]["Property ID"].ToString();
            lblAddress.Text = dt.Rows[0]["Property Address"].ToString();
            lblMuniName.Text = dt.Rows[0]["Municipality Name"].ToString();
            lblOccupancy.Text = dt.Rows[0]["Occupancy Status"].ToString();
            lblCode.Text = dt.Rows[0]["FC/FCH Codes"].ToString();
            lblServType.Text = dt.Rows[0]["VPR Service Type"].ToString();
            lblProd.Text = dt.Rows[0]["Product"].ToString();
            lblPOD.Text = dt.Rows[0]["POD"].ToString();

            if (dt.Rows[0]["Product"].ToString() == "PFC")
            {
                labelReg.Text = "PFC Registration Condition";
                lblRegCond.Text = dt.Rows[0]["PFC Registration Condition"].ToString();
            }
            else
            {
                labelReg.Text = "REO Registration Condition";
                lblRegCond.Text = dt.Rows[0]["REO Registration Condition"].ToString();
            }


            foreach (Control obj in this.Controls)
            {
                if (obj.Text == "" || obj.Text.ToUpper() == "NULL")
                {
                    obj.Text = "NULL";
                }
            }
            
        }
    }
}
