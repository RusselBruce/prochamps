﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.tbProdId = new System.Windows.Forms.TextBox();
            this.cbRegistration = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Enter PRO ID: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Select Status: ";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(262, 131);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 8;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // tbProdId
            // 
            this.tbProdId.Enabled = false;
            this.tbProdId.Location = new System.Drawing.Point(130, 93);
            this.tbProdId.Name = "tbProdId";
            this.tbProdId.Size = new System.Drawing.Size(207, 20);
            this.tbProdId.TabIndex = 7;
            // 
            // cbRegistration
            // 
            this.cbRegistration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRegistration.Items.AddRange(new object[] {
            "Successfully Padlocked",
            "Already Padlocked",
            "Already Registered",
            "Unlocked by Prochamps",
            "Reviewed by Prochamps",
            "Not for Prochamps",
            "Registration Condition Not Met",
            "Dispute Submitted",
            "Need book and page"});
            this.cbRegistration.Location = new System.Drawing.Point(130, 54);
            this.cbRegistration.Name = "cbRegistration";
            this.cbRegistration.Size = new System.Drawing.Size(207, 21);
            this.cbRegistration.TabIndex = 6;
            this.cbRegistration.SelectedIndexChanged += new System.EventHandler(this.cbRegistration_SelectedIndexChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 210);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.tbProdId);
            this.Controls.Add(this.cbRegistration);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prochamps Registration";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.TextBox tbProdId;
        private System.Windows.Forms.ComboBox cbRegistration;
    }
}