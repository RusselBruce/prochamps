﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public static string savetext = "";

        public Form3()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            clsConnection cls = new clsConnection();
            cls.setConn("DAV");

            if (comboBox1.SelectedIndex == 0)
            {
                MessageBox.Show("No option has been selected");

            }
            else
            {
                cls.ExecuteQuery("update tbl_VPR_CreatedFile set [Prochamp Status] = '" + comboBox1.SelectedItem.ToString() + "' where [id] = '" + Form1.uid + "'");
                MessageBox.Show("Status saved");
                this.Close();
            }

            
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.Items.Add("");
            
            if (Form1.screen == "pending")
            {
                comboBox1.Items.Add("Registration Completed");
                comboBox1.Items.Add("Dispute Submitted");
                comboBox1.Items.Add("Previously Processed");
            }
            else if (Form1.screen == "renewal" || Form1.screen == "continuing")
            {
                comboBox1.Items.Add("Registration Completed");
                comboBox1.Items.Add("Dispute Submitted");
                comboBox1.Items.Add("Previously Registered");
                comboBox1.Items.Add("For Delisting");
                comboBox1.Items.Add("Not for Prochamps");
                comboBox1.Items.Add("Previously Processed");
            }

            comboBox1.SelectedIndex = 0;
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }
    }
}
